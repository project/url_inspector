# Url Inspector

The `url_inspector` module is a tool designed for Search Engine Optimization (SEO).
It allows users to interact with Google's Search Console API for managing and inspecting URL
indexing status directly from their website's backend.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/url_inspector).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/url_inspector).

## Requirements

This module requires modules outside of Drupal core:

- [JSON Field](https://www.drupal.org/project/json_field)
- [Google API PHP Clien](https://www.drupal.org/project/google_api_client)

The minimum database versions are:
- MySQL v5.7.8
- MariaDB v10.2.7
- PostgreSQL v9.2
- sqlite v3.26

## Installation

```bash
  composer require drupal/url_inspect
```

```bash
  drush en url_inspector
```

## Configuration
### Create a Google Service Account

To use the module, you need to create a Google Service Account and
configure it with the necessary permissions. Follow these steps:

- Go to the Google Cloud Console.
- Create a new Service Account.
- Assign the **Search Console API** to the Service Account.
- Download the Service Account credentials JSON file.


### Configure the Google API Service Client

1. Navigate to the **Google API Service Client** configuration page:
   `/admin/config/services/google_api_service_client`.
2. Fill out the required fields:
  - **Label**: Enter a label for your service account (e.g., `SEO Service`).
  - **Credentials**: Upload the JSON file you downloaded from Google Cloud.
  - **Services**: Select `Search Console API`.
  - **Scopes**: Add `https://www.googleapis.com/auth/webmasters`.
3. Click **Save**.


### Configure Url Inspector Settings

1. Navigate to the **URL Inspector Settings** page:
   `/admin/url-inspector/configuration`.
2. Add the following information:
  - **Current URL**: Specify the URL you want to inspect or manage.
  - **Service**: Select the Google Service Account you created earlier.
3. Test the configuration by performing an index test to verify that the
   setup works correctly.


### Usage

Once the module is set up, you can:

- Inspect URLs for their indexing status in Google Search.
- Manage and test URL indexing requests directly through your website.


### Notes

- Ensure that your website has appropriate permissions to interact with
  Google's Search Console API.
- For any issues or troubleshooting, refer to the Google Cloud and Search
  Console API documentation.


### License

This project is licensed under the MIT License.
