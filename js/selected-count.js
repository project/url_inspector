/**
 * @file
 * Contains JS code to handle count of currently selected entities by type.
 */

(function ($, Drupal, once) {
  /**
   * Adds table row with a count cell.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches selectedCount behaviors.
   */
  Drupal.behaviors.selectedCount = {
    attach(context, settings) {
      /**
       * Shows number in element found by selector.
       */
      function showCount(num, selector) {
        const $elem = $(selector);
        if (num <= 0) {
          $elem.textContent = '';
        } else {
          const text = Drupal.t(
            'Inspection will be applied to @num entities.',
            { '@num': num },
          );
          $elem.textContent = text;
        }
      }

      /**
       * Perform calculation.
       */
      function doCalculate(
        tableSelector,
        countableCellIndex,
        checkBoxCellOrderNumber,
      ) {
        // Get all the selected checkboxes.
        const $checkedCheckBoxes = $(
          `${tableSelector} td:nth-child(${checkBoxCellOrderNumber}) input[type="checkbox"]:checked`,
        );
        let num = 0;
        $checkedCheckBoxes.each(function () {
          // Calculate num.
          const countableCellVal = parseInt(
            $(this).closest('tr').children().eq(countableCellIndex).textContent,
            10,
          );
          num += countableCellVal;
        });
        showCount(num, '.selected-entities-count');
      }

      /**
       * Counts by column.
       *
       * @param tableSelector
       * @param countableCellIndex
       * @param checkBoxCellOrderNumber
       */
      function countSelected(
        tableSelector,
        countableCellIndex,
        checkBoxCellOrderNumber,
      ) {
        // Calculate on load of the page + on change of checkbox.
        doCalculate(tableSelector, countableCellIndex, checkBoxCellOrderNumber);
        const $checkBoxes = $(
          once(
            'selectedCount',
            `${tableSelector} td:nth-child(${checkBoxCellOrderNumber}) input[type="checkbox"]`,
            context,
          ),
        );
        $checkBoxes.each(function () {
          const $checkBox = $(this);
          $checkBox.on('change', function () {
            // Get all the selected checkboxes.
            doCalculate(
              tableSelector,
              countableCellIndex,
              checkBoxCellOrderNumber,
            );
          });
        });
      }

      $(once('attachSelectedCount', 'body', context)).each(function () {
        if ($('table#edit-content-types').length) {
          countSelected('table#edit-content-types', 2, 1);
        }
        if ($('table#edit-vocabularies').length) {
          countSelected('table#edit-vocabularies', 2, 1);
        }
      });
    },
  };
})(jQuery, Drupal, once);
