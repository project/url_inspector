<?php

namespace Drupal\url_inspector\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure URL Inspector settings for the content types.
 */
class ContentTypesInspectionSettingsForm extends ConfigFormBase {

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * EntityTypeManager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * DatabaseConnection service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $dbConnection;

  /**
   * Constructs ContentTypesInspectionSettingsForm object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    ModuleHandlerInterface $module_handler,
    EntityTypeManagerInterface $entity_type_manager,
    Connection $db_connection,
    $typedConfigManager = NULL,
  ) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
    $this->dbConnection = $db_connection;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container): ContentTypesInspectionSettingsForm {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->has('config.typed') ? $container->get('config.typed') : NULL
    );
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames(): array {
    return ['url_inspector.settings'];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'url_inspector_content_types_settings';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    if (!$this->moduleHandler->moduleExists('node')) {
      $form['node_module_disabled'] = [
        '#type' => 'item',
        '#description' => $this->t('Node module is disabled. No configuration options are available.'),
      ];
      return $form;
    }
    $form['content_types'] = [
      '#type' => 'tableselect',
      '#header' => [
        'content_type' => $this->t('Content type'),
        'nodes_count' => $this->t('Nodes count'),
      ],
      '#options' => $this->getContentTypes(FALSE),
      '#empty' => $this->t('No content types found.'),
      '#default_value' => $this->getContentTypes(),
    ];
    $form['#attached']['library'][] = 'url_inspector/drupal.url_inspector.selected_count';
    // Prepare an element to display count of applicable entities.
    $form['selected_entities_count'] = [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#attributes' => [
        'class' => ['selected-entities-count'],
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $selected_content_types = array_filter($form_state->getValue('content_types'));

    $this->config('url_inspector.settings')
      ->set('content_types', $selected_content_types)
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Gets a list of content types.
   */
  public function getContentTypes(bool $only_configured = TRUE): array {
    if ($only_configured) {
      return $this->configFactory->get('url_inspector.settings')
        ->get('content_types') ?? [];
    }

    $all_node_types = $this->entityTypeManager->getStorage('node_type')
      ->loadMultiple();
    $nodes_counts_query = $this->dbConnection->select('node_field_data', 'nfd');
    $nodes_counts_query->fields('nfd', ['type']);
    $nodes_counts_query->addExpression('count(type)', 'type_node_count');
    $nodes_counts_query->groupBy('nfd.type');
    $query_result = $nodes_counts_query->execute()
      ->fetchAllAssoc('type', \PDO::FETCH_ASSOC);
    return array_map(static function ($node_type) use ($query_result) {
      return [
        'content_type' => $node_type->label(),
        'nodes_count' => $query_result[$node_type->id()]['type_node_count'] ?? 0,
      ];
    }, $all_node_types);
  }

}
