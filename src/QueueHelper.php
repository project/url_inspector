<?php

namespace Drupal\url_inspector;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Queue\QueueFactoryInterface;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Google\Service\SearchConsole\InspectUrlIndexResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Contains helper methods to work with module-provided queues.
 */
class QueueHelper {

  use StringTranslationTrait;

  /**
   * Constructs QueueHelper object.
   */
  public function __construct(
    private readonly QueueFactoryInterface $queueFactory,
    private readonly Connection $database,
    private readonly RequestStack $requestStack,
    private readonly ConfigFactoryInterface $configFactory,
    private readonly GoogleSearchConsoleAPI $googleSearchConsoleAPI,
    private readonly UrlInspectionOperationsManager $urlInspectionOperationsManager,
  ) {

  }

  /**
   * Inserts an item into queue if it doesn't exist yet.
   *
   * @throws \Exception
   */
  public function queueUnique(array $data, string $queue_name): void {
    if ($this->existsInQueue($data, $queue_name)) {
      return;
    }
    $this->queueFactory->get($queue_name)->createItem($data);
  }

  /**
   * Checks if item is already in queue.
   *
   * @throws \Exception
   */
  public function existsInQueue(array $data, string $queue_name, bool $return_as_id = FALSE): bool|array {
    $query = $this->database->select('queue', 'q');
    $query->condition('name', $queue_name);
    $query->condition('data', serialize($data));
    $query->fields('q', ['item_id']);

    if ($return_as_id) {
      return $query->execute()->fetchAll();
    }
    return !empty($query->execute()->fetchAll());
  }

  /**
   * Process queue item.
   *
   * @throws \JsonException
   */
  public function process($data): void {
    // Prepare required parts to get index status. Firstly check for
    // Google account. If it is not set - nothing could be fetched from API.
    $google_account = $this->configFactory->get('url_inspector.settings')->get('google_service_account');
    if (empty($google_account)) {
      throw new SuspendQueueException($this->t('No google account found.'));
    }
    $scheme_and_host = $this->requestStack->getCurrentRequest() ? $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost() : NULL;
    if (!$scheme_and_host) {
      throw new SuspendQueueException($this->t('An error occurred while determining the host.'));
    }
    $domain = parse_url($scheme_and_host, PHP_URL_HOST);
    $domain = "sc-domain:" . $domain;
    $result = $this->googleSearchConsoleAPI
      ->getIndexStatus($scheme_and_host . '/' . $data['path'], $domain, $google_account);
    if ($result instanceof InspectUrlIndexResponse) {
      $index_status_result = $result->getInspectionResult()->getIndexStatusResult();
      $data = [
        'bundle' => 'url_inspection_type',
        'label' => 'Inspection: ' . $data['entity_type_id'] . '|' . $data['entity_bundle'] . '|' . $data['entity_id'],
        'entity_identifier' => serialize([
          'entity_type_id' => $data['entity_type_id'],
          'entity_id' => $data['entity_id'],
        ]),
        'verdict_status' => VerdictType::fromName($index_status_result->getVerdict()),
        'crawled_date' => strtotime($index_status_result->getLastCrawlTime()),
        'detailed_info' => json_encode($index_status_result->toSimpleObject(), JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES),
      ];
      if ($this->urlInspectionOperationsManager->inspectionExists($data['entity_identifier'])) {
        $this->urlInspectionOperationsManager->updateInspection($data);
      }
      else {
        $this->urlInspectionOperationsManager->createInspection($data);
      }
    }
  }

}
