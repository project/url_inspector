<?php

namespace Drupal\url_inspector;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\google_api_client\Service\GoogleApiClientService;
use Drupal\google_api_client\Service\GoogleApiServiceClientService;
use Google\Service\SearchConsole;
use Google\Service\SearchConsole\InspectUrlIndexRequest;
use Google\Service\SearchConsole\RunMobileFriendlyTestRequest;

/**
 * Contains methods to work with GoogleSearchConsoleAPI.
 */
class GoogleSearchConsoleAPI {

  /**
   * The GoogleApiServiceClientService service.
   *
   * @var \Drupal\google_api_client\Service\GoogleApiServiceClientService
   */
  private $googleService;

  /**
   * The GoogleApiClientService service.
   *
   * @var \Drupal\google_api_client\Service\GoogleApiClientService
   */
  private $googleClientService;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs the MapBuilder.
   *
   * @param \Drupal\google_api_client\Service\GoogleApiClientService $google_client_service
   *   GoogleApiClientService.
   * @param \Drupal\google_api_client\Service\GoogleApiServiceClientService $google_service_client_service
   *   GoogleApiServiceClientService.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    GoogleApiClientService $google_client_service,
    GoogleApiServiceClientService $google_service_client_service,
    EntityTypeManagerInterface $entity_type_manager,
  ) {
    $this->googleService = $google_service_client_service;
    $this->googleClientService = $google_client_service;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Request mobile compatibility from Google Search Api Console.
   *
   * @param string $urlToTest
   *   The URL string.
   * @param bool $withScreenShot
   *   Make a screenshot flag.
   * @param string $googleApiClientAccount
   *   The name of a Google service account.
   *
   * @return \Google\Service\SearchConsole\RunMobileFriendlyTestResponse|false
   *   A response of the Search Console or FALSE if account is missing.
   */
  public function isUrlMobileFriendly(string $urlToTest, bool $withScreenShot, string $googleApiClientAccount) {
    $google_api_service_client = $this->entityTypeManager
      ->getStorage('google_api_service_client')
      ->load($googleApiClientAccount);

    // Check the loaded account.
    if (empty($google_api_service_client)) {
      return FALSE;
    }

    $this->googleService->setGoogleApiClient($google_api_service_client);

    $searchConsole = new SearchConsole($this->googleService->googleClient);

    $urlToBeTested = new RunMobileFriendlyTestRequest();
    $urlToBeTested->setRequestScreenshot($withScreenShot);
    $urlToBeTested->setUrl($urlToTest);

    return $searchConsole->urlTestingTools_mobileFriendlyTest->run($urlToBeTested);
  }

  /**
   * Request index status from Google Search Api Console.
   *
   * @param string $urlToTest
   *   The URL string.
   * @param string $domainToTest
   *   The domain name.
   * @param string $googleApiServiceAccount
   *   The name of a Google service account.
   *
   * @return \Google\Service\SearchConsole\InspectUrlIndexResponse|false
   *   A response of the Search Console or FALSE if account is missing.
   */
  public function getIndexStatus(string $urlToTest, string $domainToTest, string $googleApiServiceAccount) {
    $google_api_service_client = $this->entityTypeManager
      ->getStorage('google_api_service_client')
      ->load($googleApiServiceAccount);

    // Check the loaded account.
    if (empty($google_api_service_client)) {
      return FALSE;
    }

    $this->googleService->setGoogleApiClient($google_api_service_client);
    $searchConsole = new SearchConsole($this->googleService->googleClient);

    $urlIndexRequest = new InspectUrlIndexRequest();
    $urlIndexRequest->setInspectionUrl($urlToTest);
    $urlIndexRequest->setSiteUrl($domainToTest);

    return $searchConsole->urlInspection_index->inspect($urlIndexRequest);
  }

}
