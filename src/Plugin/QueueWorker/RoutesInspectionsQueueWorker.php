<?php

namespace Drupal\url_inspector\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\url_inspector\QueueHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides queue worker plugin to process routes pages using Google API.
 *
 * @QueueWorker(
 *   id = "routes_inspections",
 *   title = @Translation("Routes inspections worker")
 * )
 */
class RoutesInspectionsQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs RoutesInspectionsQueueWorker object.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    private readonly QueueHelper $queueHelper,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ): RoutesInspectionsQueueWorker {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('url_inspector.queue_helper')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function processItem($data): void {
    $this->queueHelper->process($data);
  }

}
