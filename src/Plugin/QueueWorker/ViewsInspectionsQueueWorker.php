<?php

namespace Drupal\url_inspector\Plugin\QueueWorker;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\url_inspector\GoogleSearchConsoleAPI;
use Drupal\url_inspector\UrlInspectionOperationsManager;
use Drupal\url_inspector\VerdictType;
use Drupal\url_inspector\QueueHelper;
use Google\Service\SearchConsole\InspectUrlIndexResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides queue worker plugin to process views pages using Google API.
 *
 * @QueueWorker(
 *   id = "views_inspections",
 *   title = @Translation("Views inspections worker")
 * )
 */
class ViewsInspectionsQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * Constructs ViewsInspectionsQueueWorker instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    private readonly GoogleSearchConsoleAPI $googleSearchConsoleAPI,
    private readonly RequestStack $requestStack,
    private readonly ConfigFactoryInterface $configFactory,
    private readonly UrlInspectionOperationsManager $urlInspectionOperationsManager,
    private readonly QueueHelper $queueHelper,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): ViewsInspectionsQueueWorker {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('url_inspector.google_search_console_api'),
      $container->get('request_stack'),
      $container->get('config.factory'),
      $container->get('url_inspector.url_inspection.operations_manager'),
      $container->get('url_inspector.queue_helper')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function processItem($data): void {
    // Prepare required parts to get index status. Firstly check for
    // Google account. If it is not set - nothing could be fetched from API.
    $google_account = $this->configFactory->get('url_inspector.settings')->get('google_service_account');
    if (empty($google_account)) {
      throw new SuspendQueueException($this->t('No google account found.'));
    }
    $scheme_and_host = $this->requestStack->getCurrentRequest() ? $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost() : NULL;
    if (!$scheme_and_host) {
      throw new SuspendQueueException($this->t('An error occurred while determining the host.'));
    }
    $domain = parse_url($scheme_and_host, PHP_URL_HOST);
    $domain = "sc-domain:" . $domain;
    $result = $this->googleSearchConsoleAPI
      ->getIndexStatus($scheme_and_host . '/' . $data['view_path'], $domain, $google_account);
    if ($result instanceof InspectUrlIndexResponse) {
      $index_status_result = $result->getInspectionResult()->getIndexStatusResult();
      $data = [
        'bundle' => 'url_inspection_type',
        'label' => 'Inspection: View page|' . $data['view_id'] . '|' . $data['view_display'],
        'entity_identifier' => serialize([
          'entity_type_id' => 'view',
          'entity_id' => $data['view_id'] . '|' . $data['view_display'],
        ]),
        'verdict_status' => VerdictType::fromName($index_status_result->getVerdict()),
        'crawled_date' => strtotime($index_status_result->getLastCrawlTime()),
        'detailed_info' => json_encode($index_status_result->toSimpleObject(), JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES),
      ];
      if ($this->urlInspectionOperationsManager->inspectionExists($data['entity_identifier'])) {
        $this->urlInspectionOperationsManager->updateInspection($data);
      }
      else {
        $this->urlInspectionOperationsManager->createInspection($data);
      }
    }

    $this->queueHelper->process($data);
  }

}
