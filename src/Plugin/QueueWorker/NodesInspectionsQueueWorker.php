<?php

namespace Drupal\url_inspector\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\url_inspector\QueueHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides queue worker plugin to process nodes pages using Google API.
 *
 * @QueueWorker(
 *   id = "nodes_inspections",
 *   title = @Translation("Nodes inspections worker")
 * )
 */
class NodesInspectionsQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs NodesInspectionsQueueWorker object.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    private readonly QueueHelper $queueHelper,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ): NodesInspectionsQueueWorker {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('url_inspector.queue_helper')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function processItem($data): void {
    $this->queueHelper->process($data);
  }

}
