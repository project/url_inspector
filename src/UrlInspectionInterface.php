<?php

declare(strict_types=1);

namespace Drupal\url_inspector;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining an url inspection entity type.
 */
interface UrlInspectionInterface extends ContentEntityInterface, EntityChangedInterface {

}
